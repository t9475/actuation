
   <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer__top">
        <div class="container">
          <div class="row mb-5 text-center justify-content-center">
            <div class="col-12 col-md-9 col-lg-8 col-xl-7"></div>
            <h3 class="text--light">GET 30% OFF ON YOUR FIRST ORDER</h3>
            <p class="text--light-muted mb-4">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vel, dolor?</p>
            <form action="#" class="form subscribe-form d-flex justify-content-center">
              <div class="form__inner-wrapper bg--white border-radius--custom d-flex align-items-center p-2 border-custom-1">
                <div>
                  <input type="email" name="sub_email" placeholder="Enter your email..." class="border-0 py-2 px-3" />
                </div>
                <button type="submit" class="button button-primary">
                  <span class="d-md-none"><i class="bi bi-send-fill"></i></span><span class="d-none d-md-block">Subscribe</span>
                </button>
              </div>
            </form>
          </div>
          <div class="row pt-4">
            <div class="col-12 col-md-6 col-lg-4 text-center text-md-start mb-4 mb-lg-0 pe-lg-4">
              <h6 class="text--light mb-2 mb-md-3">About Us</h6>
              <p class="text--light-muted text--sm">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Hic nostrum, doloribus perferendis quos autem cupiditate.</p>
              <ul class="social-icons d-flex justify-content-center justify-content-md-start">
                <li>
                  <a href="#" target="_blank">
                    <i class="bi bi-facebook"></i>
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="bi bi-instagram"></i>
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="bi bi-twitter"></i>
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="bi bi-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-3 text-center text-md-start mb-4 mb-lg-0 ps-xl-5">
              <h6 class="text--light mb-2 mb-md-3">Quick Links</h6>
              <ul class="site-footer-menu">
                <li class="site-footer-menu__item">
                  <a href="#" class="site-footer-menu__item--link">About Us</a>
                </li>
                <li class="site-footer-menu__item">
                  <a href="#" class="site-footer-menu__item--link">Privacy Policy</a>
                </li>
                <li class="site-footer-menu__item">
                  <a href="#" class="site-footer-menu__item--link">My Account</a>
                </li>
                <li class="site-footer-menu__item">
                  <a href="#" class="site-footer-menu__item--link">Wishlist</a>
                </li>
              </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-2 text-center text-md-start mb-4 mb-md-0">
              <h6 class="text--light mb-2 mb-md-3">Need Help?</h6>
              <ul class="site-footer-menu">
                <li class="site-footer-menu__item">
                  <a href="#" class="site-footer-menu__item--link">FAQs</a>
                </li>
                <li class="site-footer-menu__item">
                  <a href="#" class="site-footer-menu__item--link">Shipping</a>
                </li>
                <li class="site-footer-menu__item">
                  <a href="#" class="footer-menu__item--link">Privacy Policy</a>
                </li>
                <li class="site-footer-menu__item">
                  <a href="#" class="footer-menu__item--link">Return Policy</a>
                </li>
              </ul>
            </div>
            <div class="col-12 col-md-6 col-lg-3 text-center text-md-start">
              <h6 class="text--light mb-2 mb-md-3">Contact Information</h6>
              <div class="site-footer-contact-wrapper mb-3">
                <p class="text--light-muted fw--semibold mb-1">Send us an email</p>
                <a href="mailto:contact@example.com" class="d-inline-block site-footer-contact--link">contact@example.com</a>
              </div>
              <div class="site-footer-contact-wrapper">
                <p class="text--light-muted fw--semibold mb-1">Call us</p>
                <a href="tel:+123 567890" class="d-inline-block site-footer-contact--link">+123 567890</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="site-footer__bottom bg-dark pt-3 py-4 p-md-3">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-lg-6 mb-3 mb-lg-0">
              <p class="text--light-muted m-0 text--sm text-center text-lg-start">© Copyright 2022 www.actuationtestequipment.com. All Rights Reserved.</p>
            </div>
            <div class="col-12 col-lg-6 text-center text-lg-end">
              <img src="<?php echo base_url('assets/images/PAY.png')?>" alt="Payment Methods" class="d-inline-block" width="230" />
            </div>
          </div>
        </div>
      </div>
      <div class="position-fixed p-3 toast-container" style="z-index: 11">
        <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true">
          <div class="toast-header justify-content-end px-2 pt-2 pb-0">
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
          </div>
          <div class="toast-body p-3">
            <span class="toast-body__icon me-2"></span>
            <span class="toast-body__text text--heading"></span>
          </div>
        </div>
      </div>
      <a href="javascript:void(0)" class="scroll-to-top-button"><i class="bi bi-arrow-up"></i></a>
    </footer>
    <script src="<?php echo base_url('assets/vendors/bootstrap-5.0.2-dist/js/bootstrap.bundle.min.js')?>"></script>
    <script src="<?php echo base_url('assets/vendors/jquery.js')?>"></script>
    <script src="<?php echo base_url('assets/vendors/custom-dropdown-from-select-pure-js/dist/script.js')?>"></script>
    <script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-image-zoom/js-image-zoom.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
    <script src="<?php echo base_url('assets/vendors/youtube-video-popup/youtube-overlay.js')?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery.counterup@2.1.0/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url('assets/js/script.js')?>"></script>
  </body>
</html>
