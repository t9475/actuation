<?php $this->load->view('template/header');?>

<main class="site-content">
      <section class="page-title-section d-flex justify-content-center align-items-center">
        <div class="container">
          <h3 class="page-title text-center">Products</h3>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb d-flex justify-content-center p-0">
              <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
          </nav>
        </div>
      </section>
      <section class="section--padding product-archive">
        <div class="container">
            <div class="row">
              <?php foreach($data['products'] as $products):?>
                    <div class="col-6 col-md-4 col-xl-3">
                        <div class="product-card border-radius--custom h-100">
                            <a href="<?php echo base_url('home/product_details/'.$products['id']);?>" class="product-card__thumbnail-container text-center p-3 d-block" style="height:240px">
                            <img style="height:240px" src="<?php print_r(base_url('assets/images/products/'.json_decode($products['img'], TRUE)['images_0']));?>" alt="ATE-37 Power Amplifier" class="d-inline-block" />
                            </a>
                            <div class="product-card__body p-3">
                                <div class="product-card__title text--heading mt-4 text-center">
                                    <a href="<?php echo base_url('home/product_details/'.$products['id']);?>"><?php echo $products['name']?></a>
                                </div>
                                <div class="product-card__price">
                                    <p class="text--secondary text-center"><?php echo "$".$products['selling_price'].".00"?> <span class="ms-3 text--para text-decoration-line-through"><?php echo "$".$products['mrp'].".00"?></span> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
          </div>
        </div>
      </section>
    </main>
<?php $this->load->view('template/footer');?>