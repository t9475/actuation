    <?php $this->load->view('template/header');?>
        <main class="site-content">
            <section class="page-title-section d-flex justify-content-center align-items-center">
                <div class="container">
                <h3 class="page-title text-center">Login</h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb d-flex justify-content-center p-0">
                    <li class="breadcrumb-item"><a href="">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Login</li>
                    </ol>
                </nav>
                </div>
            </section>
            <section class="user-action-section user-action-section--login section--padding position-relative bg--light">
                <div class="container">

                    <?php  if ($error = $this->session->flashdata('message_name')) { ?>
                        <div class="row">
                            <div class="col-lg-12 col-xs-12">
                                <div class="alert alert-danger">
                                    <?php echo $error; ?>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-9 col-lg-11 col-xl-9 col-xxl-8">
                            <div class="form-container form-container--login row box-shadow--custom border-radius--custom mx-1 mx-md-0">
                                <div class="col-12 col-md-12 col-lg-6 form-container__img-col"></div>
                                <div class="col-12 col-md-12 col-lg-6 form-container__content-col px-3 py-4 p-md-4 p-lg-5 bg--white">
                                    <h4 class="text--heading font--serif mb-3">Login</h4>
                                    <?php echo form_open_multipart('home/user_login', array('class'=>'form login-form', 'autocomplete'=>"off"));?>
                                        <div class="form__field">
                                            <label for="username" class="d-block mb-2 text--heading">Email*</label>
                                            <?php echo form_input(['name'=>'l_email', 'class'=> 'bg--light','autocomplete'=>'new-password'])?>
                                        </div>
                                        <div class="form__field mb-4">
                                            <label for="password" class="d-block mb-2 text--heading">Password*</label>
                                            <?php echo form_input(['name'=> 'l_password', 'type'=>'password', 'class'=> 'bg--light', 'autocomplete'=> 'new-password'])?>
                                            <button class="button--show-password p-1"><i class="bi bi-eye"></i></button>
                                        </div>
                                        <button type="submit" class="button button-primary d-block w-100">Login</button>
                                    <?php echo form_close();?>
                                    <p class="text--para mt-3 mb-0 text-center">Don't have an account? <a href="<?php echo base_url('home/register_page')?>" class="redirecting-link--register fw-semibold text-decoration-underline">Register Now</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    <?php $this->load->view('template/footer');?>

    <script type="application/javascript">
        $(window).bind("load", function() {
            window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
            }, 1000);
        });
    </script>