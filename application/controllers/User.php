<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class User extends MY_Controller{
        public function login_check(){
            if (!$this->session->userdata('user_id')) {
                return redirect(base_url('/Home'));
            }else{
                $is_user_login = 1;
                $user_id = $this->session->userdata('user_id');
            }
        }

        public function user_login(){
            $this->form_validation->set_rules('l_email','Email Id', 'required | valid_email');
            $this->form_validation->set_rules('password','Password', 'required');
            if($this->form_validation->run()){
                $data['email_addr'] = $this->input->post('l_email');
                $data['password'] = md5($this->input->post('password'));
                $data['current_url'] = $this->input->post('current_url');
                $data_login = $this->User_model->userLoginCheck($data);
                echo "<pre>";
                print_r($this->db->last_query());
                print_r($data_login);

            }else{
                $this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
				
            }
        }

        public function update_add_to_cart(){
            $check_cart_count = $this->check_cart_by_user_id($this->input->post('product_id'), $this->input->post('user_id'));
            $data['product_id'] = $this->input->post('product_id');
            $data['user_id'] = $this->input->post('user_id');
            if($this->input->post('qty')){
                $data['qty'] = $this->input->post('qty');
            }else{
                $data['qty'] = '1';
            }
            $this->User_model->update_add_to_cart($data, $check_cart_count);
        }

        private function check_cart_by_user_id($product_id, $user_id){
            return $this->User_model->check_cart_by_user_id($product_id, $user_id);
        }

        public function cart(){
			$this->login_check();
			$user_id = $this->session->userdata('user_id');
			$wishlist_product = $this->User_model->get_data_from_cart_by_user_id($user_id);
            $user_address = $this->User_model->get_address_by_user_id($user_id);
			$data['data'] = array('cart_product'=>$wishlist_product, 'user_address' => $user_address);
			$this->load->view('frontend/user_dashboard/cart', $data);
		}

         public function deletecartproduct($id){
            $this->User_model->delete_cart($id);
            return redirect('user/cart');
        }

        public function deleteAllCartProduct($user_id){
            $this->User_model->delete_all_cart($user_id);
            return redirect('user/cart');
        }

        public function add_address_init(){
            $data['address'] = $this->input->post('address');
            $data['zip_code'] = $this->input->post('zip_code');
            $data['city'] = $this->input->post('city');
            $data['landmark'] = $this->input->post('landmark');
            $data['is_default'] = 1;
            $data['user_id'] = $this->session->userdata('user_id');
            $this->User_model->removeDefaultAddress($this->session->userdata('user_id'));
            $this->User_model->add_new_address($data);
            $get_last_entry_id = $this->User_model->getLastAddressId();
            $latest_default_address_id['address'] =  $get_last_entry_id[0]['id'];
            $update_user_address = $this->User_model->update_user_address($this->session->userdata('user_id'), $latest_default_address_id);
            return redirect('user/cart');
        }


    }
?>