<?php 
	class Admin extends MY_Controller{
		function __construct() {
			parent::__construct();
			$method=  $this->router->fetch_method();
			$allowed=array('index');
			if(!in_array($method,$allowed)){
				$this->login_check();
			}
		}

		public function login_check(){
			if (!$this->session->userdata('user') || !$this->session->userdata('id')) {
				return redirect(base_url('admin'));
			}
			if ($this->session->userdata('user') == "Admin") {
				$is_admin = 1;
			}elseif ($this->session->userdata('user') == "User") {
				$is_admin = 0;
			}
		}
		
		public function index(){
			if ($this->session->userdata('user')) {
				redirect("/admin/home");
			}else{
				$this->form_validation->set_rules('uname','User Name','required|valid_email');
                $this->form_validation->set_rules('password','Password','required');
                $this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
                if($this->form_validation->run()){
                    $user_name = $this->input->post('uname');
                    $password = md5($this->input->post('password'));
                    $login = $this->Admin_model->admin_login($user_name,$password);
                    if($login){
                        $this->session->set_userdata('user',$login['user']);
                        $this->session->set_userdata('id', $login['user_id']);
                        return redirect('/admin/index');
                    }else{
                        $this->session->set_flashdata('Login_failed','Invalid Username/Password');
                        return redirect(base_url('admin'));
                    }
                }else{
                    $this->load->view("/admin/signin");
                }
			}
		}

		public function logout(){
			if($this->session->userdata('user')){
	            $this->session->unset_userdata('user');
	            $this->session->unset_userdata('id');
	            return redirect(base_url('admin'));
            }else{
                return redirect('/home/login');
            }
		}

		public function home(){
			$this->login_check();
			$this->load->view("/admin/index");
		}

		public function add_product_init(){
			$this->login_check();		
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('mrp', 'MRP','required');
			$this->form_validation->set_rules('selling_price', 'selling Price','required');
			
			if ($this->form_validation->run()) {

				$product = array();
				$product['name'] = $this->input->post('name');
				$product['mrp'] = $this->input->post('mrp');
				$product['selling_price'] = $this->input->post('selling_price');
				$product['description'] = $this->input->post('description');
				$product['status'] = 1;

				$files = $_FILES;
				$cpt = count($_FILES['userfile']['name']);
				for($i=0; $i<$cpt; $i++){           
					$_FILES['userfile']['name']= $files['userfile']['name'][$i];
					$_FILES['userfile']['type']= $files['userfile']['type'][$i];
					$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
					$_FILES['userfile']['error']= $files['userfile']['error'][$i];
					$_FILES['userfile']['size']= $files['userfile']['size'][$i];    

					$this->upload->initialize($this->image_config());
					$this->upload->do_upload('userfile');
					$data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
				}
				$product['img'] = json_encode($data_media['img']);
				
				$this->Admin_model->product_insert($product);
				return redirect('/admin/products');
			}else{
				$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
				$this->load->view("/admin/product_upload");
			}
		}

		private function image_config(){
			$config = array();
			$config['upload_path'] = 'assets/images/products/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']      = '0';
			$config['overwrite']     = FALSE;
			$config['remove_spaces'] = true;
			$config['encrypt_name'] = true;

			return $config;
		}

		public function add_product(){
			$this->login_check();		
			$this->load->view("/admin/product_upload");
		}

		public function products(){
			$products['data'] = $this->Admin_model->allCourses();
			$this->load->view('/admin/products', $products);
		}

		public function edit_course($id){
			$product = $this->Admin_model->selectCourseById($id);
			$all_data['data'] = array('product' => $product);
			$this->load->view('/admin/edit_course', $all_data);
		}

		public function do_edit_course(){
			$this->login_check();		
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('mrp', 'MRP','required');
			$this->form_validation->set_rules('selling_price', 'selling Price','required');
			
			if ($this->form_validation->run()) {
				$product = array();
				$product['name'] = $this->input->post('name');
				$product['mrp'] = $this->input->post('mrp');
				$product['selling_price'] = $this->input->post('selling_price');
				$product['description'] = $this->input->post('description');

				if(count($_FILES['userfile']['name']) > 1){
					$files = $_FILES;
					$cpt = count($_FILES['userfile']['name']);
					for($i=0; $i<$cpt; $i++){           
						$_FILES['userfile']['name']= $files['userfile']['name'][$i];
						$_FILES['userfile']['type']= $files['userfile']['type'][$i];
						$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
						$_FILES['userfile']['error']= $files['userfile']['error'][$i];
						$_FILES['userfile']['size']= $files['userfile']['size'][$i];    

						$this->upload->initialize($this->image_config());
						$this->upload->do_upload('userfile');
						$data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
					}
				}
				if(!empty($data_media['img'])){
					$product['img'] = json_encode($data_media['img']);
				}
				

				$this->Admin_model->update_course_by_id($this->input->post('id'),$product);
				return redirect('/admin/products');
			}else{
				$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
				$this->load->view("/admin/product_upload");
			}
		}

		public function delete_course($id){
			$this->login_check();
			$this->Admin_model->delete_product($id);
			return redirect('admin/products');
		}

		public function users(){
			$data['data'] = array('all_user'=> $this->Admin_model->Allusers());
			$this->load->view('/admin/users/users', $data);
		}

		public function order(){
			$data['data'] = array('orders'=> $this->Admin_model->orders());
			$this->load->view('admin/order', $data);
		}

		public function approveOrder($id){
			$data = array('order_status' => 'Approved');
			$this->Admin_model->do_edit_order($id, $data);
			return redirect('admin/order');
		}

		public function reject_order($id){
			$data = array('order_status' => 'Rejected');
			$this->Admin_model->do_edit_order($id, $data);
			return redirect('admin/order');
		}

 	}
 ?>