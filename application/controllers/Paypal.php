<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paypal extends MY_Controller {

    function  __construct() {
        parent::__construct();
        $this->load->library('paypal_lib');
        //$this->load->model('product');
        $this->load->database();
    }

    // public function index(){
    //     $data = array();
    //     //get products inforamtion from database table
    //     $data['products'] = $this->Paypal_model->getRows();
    //     //loav view and pass the products information to view
    //     $this->load->view('products/index', $data);
    // }

    public function place_order(){
        $variable = $this->input->post();
        foreach ($variable as $key => $value) {
            $data_product_id[] = $value;
            $data_product_qty['product_qty'] = $value;
        }
        
        for($i = 0; $i< count($data_product_id[0]); $i++){
            $productId = $data_product_id[0];
            $get_product_price_by_id = $this->User_model->get_product_details_by_id($productId[$i]);
            $product_total_price[] = $get_product_price_by_id[0]['selling_price'] * $data_product_qty['product_qty'][$i];
            $product_details[] = array('name'=>$get_product_price_by_id[0]['name'], 'image'=> $get_product_price_by_id[0]['img']);
        }
        echo "<pre>";
        print_r(array_sum($product_total_price));
        echo "<br>";
        print_r(json_encode($product_details));
        echo "<br>";
        //print_r($product_details);
        echo "<br>";
        $get_user_details_by_id = $this->User_model->get_user_data_by_id($this->session->userdata('user_id'));
        print_r($get_user_details_by_id[0]);

        $data['product'] = $this->input->post('product_details');
        $data['user_id'] = $this->input->post('user_id');
        $data['total_price'] = $this->input->post('total_amount');
        $data['address_id'] = $this->input->post('address');
        $data['order_status'] = "Pending";
    }




    public function buyProduct(){

        $variable = $this->input->post();
        foreach ($variable as $key => $value) {
            $data_product_id[] = $value;
            $data_product_qty['product_qty'] = $value;
        }
        
        for ($i = 0; $i< count($data_product_id[0]); $i++) {
            $productId = $data_product_id[0];
            $get_product_price_by_id = $this->User_model->get_product_details_by_id($productId[$i]);
            $product_total_price[] = $get_product_price_by_id[0]['selling_price'] * $data_product_qty['product_qty'][$i];
            $product_details[] = array('id'=>$get_product_price_by_id[0]['id'],'name'=>$get_product_price_by_id[0]['name'], 'image'=> $get_product_price_by_id[0]['img'], 'selling_price' => $get_product_price_by_id[0]['selling_price'], 'qty' => $data_product_qty['product_qty'][$i]);
        }
        $get_user_details_by_id = $this->User_model->get_user_data_by_id($this->session->userdata('user_id'));

        $data['total_price'] = array_sum($product_total_price);
        $data['product'] = json_encode($product_details);
        $data['user_id'] = $this->session->userdata('user_id');
        $data['address_id'] = $get_user_details_by_id[0]['address'];
        // $order_details['user_details'] = $get_user_details_by_id[0];
        $data['order_status'] = "Pending";

        $this->Paypal_model->add_order($data);
        $get_last_order = $this->Paypal_model->get_last_order();
        // echo '<pre>';
        // print_r($get_last_order[0]['id']);exit();


        $returnURL = base_url().'paypal/success';
        $cancelURL = base_url().'paypal/cancel/'.$get_last_order[0]['id'];
        $notifyURL = base_url().'paypal/ipn';

        $userID = $this->session->userdata('user_id');
        $logo = base_url().'Your_logo_url';
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', $data['product']);
        $this->paypal_lib->add_field('custom', $userID);
        $this->paypal_lib->add_field('item_number',  "Test");
        $this->paypal_lib->add_field('amount',  $data['total_price']);        
        $this->paypal_lib->image($logo);
        $this->paypal_lib->paypal_auto_form();

    }

    public function success(){
        $paypalInfo = $this->input->get();
        $data['item_number'] = $paypalInfo['item_number']; 
        $data['txn_id'] = $paypalInfo["tx"];
        $data['payment_amt'] = $paypalInfo["amt"];
        $data['currency_code'] = $paypalInfo["cc"];
        $data['status'] = $paypalInfo["st"];
        $this->load->view('paypal/success', $data);
        $this->Paypal_model->clear_cart($this->session->userdata('user_id'));
    }

    public function cancel($id = NULL){
        $this->Paypal_model->remove_order_by_id($id);
        $this->load->view('paypal/cancel');

    }

    public function ipn(){
        //paypal return transaction details array
        $paypalInfo    = $this->input->post();
        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id']    = $paypalInfo["item_number"];
        $data['txn_id']    = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["mc_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status']    = $paypalInfo["payment_status"];
        $paypalURL = $this->paypal_lib->paypal_url;        
        $result    = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        //check whether the payment is verified
        if(preg_match("/VERIFIED/i",$result)){
            //insert the transaction data into the database
            $this->product->storeTransaction($data);
        }

    }

}