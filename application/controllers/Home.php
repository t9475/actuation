<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
		// function __construct() {
		// 	parent::__construct();
		// 	$method=  $this->router->fetch_method();
		// 	$allowed=array('index');
		// 	if(!in_array($method,$allowed)){
		// 		$this->login_check();
		// 	}
		// }

	public function login_check(){
		if (!$this->session->userdata('user_id')) {
			return redirect(base_url('/Home'));
		}else{
			$is_user_login = 1;
			$user_id = $this->session->userdata('user_id');
		}
	}

	public function index(){
		$data['data'] = array('products'=> $this->User_model->getAllCourses());
		$this->load->view('frontend/home', $data);
	}

	public function teachers(){
		$this->load->view('frontend/teachers');
	}

	public function products(){
		$data['data'] = array('products'=> $this->User_model->getAllCourses());
		$this->load->view('frontend/products/products', $data);
	}

	public function product_details($id){
		$data['data'] = array('product'=> $this->User_model->getAllCoursesById($id));
		$this->load->view('frontend/products/product_single', $data);
	}

	public function checkout($id){
		$data['data'] = array('course'=> $this->User_model->getAllCoursesById($id));
		$this->load->view('frontend/courses/checkout', $data);
	}
	
	public function login_page(){
		if ($this->session->userdata('user_id')) {
			return redirect('home/user_dashboard');
		}else{
			$this->load->view('frontend/user_dashboard/login');
		}
	}

	public function user_login(){
		$data['email_addr'] = $this->input->post('l_email');
		$data['password'] = md5($this->input->post('l_password'));
		$data['status'] = 1;
		$data_login = $this->User_model->userLoginCheck($data);

		if($data_login){
			$this->session->set_userdata('user_type', $data_login['user']);
			$this->session->set_userdata('user_id', $data_login['user_id']);
			return redirect('home/user_dashboard');
		}else{
			$message =  "Invalid Username/Password";
			$this->session->set_flashdata('message_name', $message);
			return redirect('home/login_page', 'refresh');
		}
	}

	public function logout(){
		if($this->session->userdata('user_type')){
			$this->session->unset_userdata('user_type');
			$this->session->unset_userdata('user_id');
			return redirect(base_url('/'));
		}else{
			return redirect('/');
		}
	}


	public function checkout_complete(){
		$data['user_id'] = $this->input->post('user_id');
		$data['product_id'] = $this->input->post('product_id');
		$data['img'] = $this->input->post('product_img');
		$get_product_details = $this->User_model->get_product_details_by_id($this->input->post('product_id'));
		$data['tax'] = 10;
		$data['total_price'] = $get_product_details[0]['selling_price']+$data['tax'];
		if($this->input->post('payment_mode')){
			$data['payment_mode'] = $this->input->post('payment_mode');
		}else{
			$data['payment_mode'] = "Not Paid";
		}
		$this->User_model->order_Complete($data);
		return redirect('home/user_dashboard');
	}

	public function user_dashboard(){
		$this->login_check();
		$user_id = $this->session->userdata('user_id');
		$user_details = $this->User_model->get_user_by_id($user_id);
		$user_orders = $this->User_model->get_orders_by_id($user_id);
		$user_address = $this->User_model->get_address_by_user_id($user_id);

		$data['data'] = array(['user_details'=> $user_details, 'user_orders' => $user_orders], 'user_address'=> $user_address);
		$this->load->view('frontend/user_dashboard/index', $data);
	}

	public function order_details($id){
		$order_details = $this->User_model->order_details($id);
		$data['data'] = array('order_details' => $order_details);
		$this->load->view('frontend/user_dashboard/order_details', $data);
	}

	public function register_page(){
		$this->load->view('frontend/user_dashboard/register');
	}

	public function user_register(){
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email_addr', 'Email', 'required|valid_email|is_unique[users.email_addr]');
		$this->form_validation->set_rules('mobile_number', 'Mobile Number','required|numeric|min_length[10]|max_length[10]|is_unique[users.mobile_number]');
		$this->form_validation->set_rules('password', 'Password','required');
		if($this->form_validation->run()){
			$data['name'] = $this->input->post('name');
			$data['email_addr'] = $this->input->post('email_addr');
			$data['mobile_number'] = $this->input->post('mobile_number');
			$data['user_type'] = "User";
			$data['password'] = md5($this->input->post('password'));
			$data['status'] =1;
			$this->User_model->add_user($data);
			return redirect('/home/login_page');

		}else{
			$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
			$this->load->view("frontend/user_dashboard/register");
		}
	}

	public function about_us(){
		$this->load->view('frontend/about_us');
	}

		



}
