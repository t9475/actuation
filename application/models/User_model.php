<?php 
    class User_model extends CI_model{
        public function getAllCourses(){
            return $this->db->get('products')->result_array();
        }

        public function getAllCoursesById($id){
            return $this->db->where('id', $id)
                        ->get('products')->result_array();
        }

        public function userLoginCheck($data){
            $user = $this->db->where($data)
                        ->get('users');
            if($user->num_rows() > 0){
                return array('user' => $user->row()->user_type, 'user_id' => $user->row()->id);
            }else{
                false;
            }
        }

        public function get_product_details_by_id($id){
            return $this->db->where('id', $id)
                            ->get('products')->result_array();

        }

        public function order_Complete($data){
            $this->db->insert('orders', $data);

        }

        public function get_user_by_id($user_id){
            return $this->db->where('id', $user_id)
                            ->get('users')->result_array();

        }

        public function get_orders_by_id($user_id){
            return $this->db->where('user_id', $user_id)
                            -> order_by('id', 'DESC')  
                            ->get('orders')->result_array();

        }

        public function order_details_bu_user_id($id){
            return $this->db->query("SELECT * FROM orders LEFT JOIN products ON orders.product_id = products.id where `user_id` = $id")->result_array();
        }

        public function add_user($data){
            $this->db->insert('users', $data);
        }

         public function check_cart_by_user_id($product_id, $user_id){
            return $this->db->where('user_id', $user_id)
                    ->where('product_id', $product_id)
                    ->get('cart')->num_rows();
        }

         public function update_add_to_cart($data , $already_in_cart){
            $user_id = $data['user_id'];
            $product_id = $data['product_id'];
            $product_qty = $data['qty'];
            if($already_in_cart> 0){
                $this->db->query("UPDATE cart SET qty = (qty + $product_qty)WHERE user_id = $user_id AND product_id = $product_id");
            }else{
                $this->db->insert('cart', $data);
            }
        }

        public function get_data_from_cart_by_user_id($id){
            return $this->db->query("SELECT * , products.id as product_id FROM products LEFT JOIN cart ON products.id = cart.product_id where cart.user_id =$id")->result_array();
        }

         public function delete_cart($id){
            $this->db->where('id', $id)
                    ->delete('cart');
        }

         public function delete_all_cart($id){
            $this->db->where('user_id', $id)
                    ->delete('cart');
        }

        public function get_address_by_user_id($id){
            return $this->db->where('user_id', $id)
                            ->get('address')->result_array();
        }

        public function add_new_address($data){
            $this->db->insert('address', $data);
        }

        public function removeDefaultAddress($user_id){
            $this->db->query("UPDATE `address` SET `is_default` = '0' WHERE `address`.`user_id` = $user_id");
        }

        public function addDefaultAddress($address_id, $user_id){
            $this->db->query("UPDATE `address` SET `is_default` = '1' WHERE `address`.`id` = $address_id  AND user_id = $user_id");
        }

        public function getUserAddress($id){
            return $this->db->where('id', $id)
                    ->get('address')->result_array();
        }

        public function editUserAddress($data, $id){
            $this->db->where('id', $id)
                    ->update('address', $data);
        }

        public function getLastAddressId(){
            return $this->db->order_by('id', 'DESC')
                            ->limit(1)
                            ->get('address')->result_array();
        }

        public function update_user_address($id, $address_id){
            $this->db->where('id', $id)
                    ->update('users', $address_id);
        }

        public function order_details($id){
            return $this->db->query("SELECT *,orders.id as order_id FROM orders LEFT JOIN address ON orders.address_id = address.id where orders.id = $id")->result_array();
        }



















        



















        public function getSingleProduct($id){
            return $this->db->where('id', $id)
                        ->get('products')->row();
        }
        
        public function getAllproduct($place_id){
            return $this->db->where('place_id', $place_id)
                        ->get('products')->result_array();
        }

        public function getItemCount($product_category_name, $place_id){
            return $this->db->where('place_id', $place_id)
                            ->where('product_type', $product_category_name)
                            ->get('products')->result();
        }

        public function productWithCategory($place_id){
            // $this->db->select('*');
            // $this->db->from('product_category');
            // $this->db->join('products', 'products.product_type = product_category.name');
            return $query = $this->db->query("SELECT *,products.id as product_id FROM products LEFT JOIN product_category ON products.product_type = product_category.id where `place_id` = $place_id")->result_array();
        }

        public function category_check(){
            
        }

        public function userAddress($id){
            return $this->db->where('user_id', $id)
                            ->get('address')->result_array();
        }

        

        public function edit_Personal_details($id, $data){
            $this->db->where('id', $id);
			$this->db->update('users', $data);
        }

        public function delete_user_address($id){
            $this->db->where('id', $id)
						->delete('address');
        }

        public function allorder($id){
            return $this->db->where('user_id', $id)
                            ->get('orders')->result_array();
        }

        

        public function orderProductDetails($id){
            return $this->db->where('id', $id)
                            ->get('products')->result_array();
        }

        public function getOrderProductDetails($product_id){
            return $this->db->where('id', $product_id)
                            ->get('products')->result_array();
        }

        public function check_userNumber($number){
            return $this->db->where('mobile_number', $number)
                            ->get('users')->num_rows();
        }

        public function create_user($data){
            $this->db->insert('users', $data);
        }

        public function getTheLoginUser($user_number){
            return $this->db->where('mobile_number', $user_number)
                        ->get('users')->result_array();     
        }


        public function place_order($data){
            $this->db->insert('orders', $data);
        }

        public function get_last_order(){
			return $this->db->order_by('id', 'desc')
					->limit(1)
					->get('orders')->result_array();
		}

        public function getAllUserOrders($order_id){
            return $this->db->query("SELECT * FROM orders where id = $order_id ")->result_array();
        }

        public function getProductDetailsByid($id){
            return $this->db->query("SELECT * FROM orders where id = $order_id ")->result_array();
        }

        public function getAddressByIdIsDefault($user_id){
            return $this->db->where('user_id', $user_id)
                    ->where('is_default', 1)
                    ->get('address')->num_rows();
        }

        public function makeDefaultAddressAfterDelete($data, $user_id){
            $this->db->where('user_id', $user_id)
                    ->update('address', $data);
        }

        public function get_user_data_by_id($id){
            return $this->db->where('id', $id)
                    ->get('users')->result_array();
        }

        public function insert_payment_data($data){
            $this->db->insert('payment', $data);
        }

    }
?>