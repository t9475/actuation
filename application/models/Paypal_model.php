<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paypal_model extends CI_Model{

    // public function __construct(){
    //     $this->load->database();
    // }

    //get and return product rows
    public function getRows($id = ''){
        $this->db->select('*');
        $this->db->from('products');
        if($id){
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('name','asc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }

    //insert transaction data
    public function storeTransaction($data = array()){
        $insert = $this->db->insert('payments',$data);
        return $insert?true:false;
    }

    //insert transaction data
    public function getProductById($id){
    return $this->db->where('id', $id)
                        ->get('products')->result_array();
    }

    public function add_order($data){
        $this->db->insert('orders', $data);
    }

    public function get_last_order(){
        return $this->db->order_by('id', 'DESC')
                    ->limit(1)
                ->get('orders')->result_array();
    }

    public function remove_order_by_id($id){
        $this->db->where('id', $id)
                 ->delete('orders');
    }

    public function clear_cart($id){
        $this->db->where('user_id', $id)
                ->delete('cart');
    }

}