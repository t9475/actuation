$(document).ready(function () {
  $(window).scroll(function () {
    //navbar shrink
    if ($(document).scrollTop() > 50) {
      $(".site-header").addClass("site-header--shrinked");
    } else {
      $(".site-header").removeClass("site-header--shrinked");
    }

    // Scroll Top fade in out
    if ($(document).scrollTop() > 200) {
      $(".scroll-to-top-button").addClass("scroll-to-top-button--show");
    } else {
      $(".scroll-to-top-button").removeClass("scroll-to-top-button--show");
    }
  });
  //Click event to scroll to top
  $(".scroll-to-top-button").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 500);
    return false;
  });
  // adding active class to nav links as per page
  var path = location.href.replace(location.origin, "."); // because the 'href' property of the DOM element is the absolute path
  $(".site-header ul li a").each(function () {
    $(this).removeClass("active");
    var getHref = this.href.replace(location.origin, ".").replace("#", "");
    if (getHref === "./index.html" || getHref === "/") {
      $(".site-header ul li a:first").addClass("active");
    }
    if (getHref == path) {
      $(this).addClass("active");
    }
  });
  // hero section swiper
  var swiperHero = new Swiper(".swiper-hero", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: true,
    pauseOnMouseEnter: true,
    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });
  // featued products swiper
  var swiperFeaturedProducts = new Swiper(".swiper-featured-products", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: true,
    spaceBetween: 24,
    pauseOnMouseEnter: true,
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
      // when window width is >= 480px
      576: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
      767: {
        slidesPerView: 3,
        spaceBetween: 24,
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 24,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 24,
      },
    },
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });
  //testimonial section swiper
  var swiperTestimonial = new Swiper(".swiper-testimonial", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: {
      delay: 5000,
      pauseOnMouseEnter: true,
    },
    breakpoints: {
      768: {
        slidesPerView: 1,
        spaceBetween: 15,
      },
      992: {
        slidesPerView: 2,
        spaceBetween: 15,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
    },
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });
  // Product single page swiper gallery
  var swiper = new Swiper(".product-single-swiper-thumb", {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
  });
  var swiper2 = new Swiper(".product-single-swiper-view", {
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    thumbs: {
      swiper: swiper,
    },
  });

  // show password button function
  var showPasswordButtons = $(".form .button--show-password");
  var showPassFlag = false;
  showPasswordButtons.each(function () {
    $(this).on("click", function (e) {
      e.preventDefault();
      if (!showPassFlag) {
        $(this).siblings("input").attr("type", "text");
        $(this).find("i").removeClass("bi-eye").addClass("bi-eye-slash");
        showPassFlag = true;
      } else {
        $(this).siblings("input").attr("type", "password");
        $(this).find("i").removeClass("bi-eye-slash").addClass("bi-eye");
        showPassFlag = false;
      }
    });
  });

  //Validation for Register Form
  $("#registerForm").validate({
    rules: {
      r_fullname: {
        required: true,
        patternRegFullname: /^[a-zA-Z\s]*$/,
      },
      r_email: {
        required: true,
        email: true,
      },
      r_phone: {
        required: true,
        patternPhone: /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/,
      },
      r_password: {
        required: true,
        minlength: 6,
        maxlength: 15,
      },
      r_confpassword: {
        required: true,
        equalTo: '[name="r_password"]',
      },
    },
    messages: {
      r_fullname: {
        required: "This field is required.",
      },
      r_email: {
        required: "This field is required.",
        email: "Please enter a valid email address.",
      },
      r_password: {
        required: "This field is required.",
        minlength: "Password must be minumum 6 character long.",
        maxlength: "Password must not exceed than 12 character",
      },
      r_confpassword: {
        required: "This field is required.",
        equalTo: "Password doesn't match.",
      },
    },
    submitHandler: function (form, e) {
      e.preventDefault();
      form.reset();
      notificationToastUpdate("success", "Your account has been created");
      // form.submit();
    },
  });
  $.validator.addMethod(
    "patternRegFullname",
    function (value, element, regexp) {
      return this.optional(element) || regexp.test(value);
    },
    "Number can not be included in Full Name."
  );
  $.validator.addMethod(
    "patternPhone",
    function (value, element, regexp) {
      return this.optional(element) || regexp.test(value);
    },
    "Please enter a valid phone number."
  );

  // validation login form
  $("#loginForm").validate({
    rules: {
      l_email: {
        required: true,
        email: true,
      },
      l_password: {
        required: true,
      },
    },
    messages: {
      l_email: {
        required: "This field is required.",
        email: "Please enter a valid email address.",
      },
      l_password: {
        required: "This field is required.",
      },
    },
    submitHandler: function (form, e) {
      e.preventDefault();
      form.reset();
      notificationToastUpdate("success", "You're successfully logged in.");
      // form.submit();
    },
  });

  // quanity increment and decrement funciton
  $(".qtyplus").each(function () {
    // Get the field name
    $(this).on("click", function () {
      fieldName = $(this).attr("field");
      // Get its current value
      var currentVal = parseInt($("input[name=" + fieldName + "]").val());
      // If is not undefined
      if (!isNaN(currentVal)) {
        // Increment
        $("input[name=" + fieldName + "]").val(currentVal + 1);
        $("input#add_to_cart_qty").attr('data-qty', currentVal + 1);
      } else {
        // Otherwise put a 0 there
        $("input[name=" + fieldName + "]").val(0);
      }
    });
  });
  $(".qtyminus").each(function (index, el) {
    $(this).on("click", function () {
      // Get the field name
      fieldName = $(this).attr("field");
      // Get its current value
      var currentVal = parseInt($("input[name=" + fieldName + "]").val());
      // If it isn't undefined or its greater than 0
      if (!isNaN(currentVal) && currentVal > 0) {
        // Decrement one
        $("input[name=" + fieldName + "]").val(currentVal - 1);
        $("input#add_to_cart_qty").attr('data-qty', currentVal - 1);
      } else {
        // Otherwise put a 0 there
        $("input[name=" + fieldName + "]").val(0);
      }
    });
  });
  //notification toast function for global
  function notificationToastUpdate(msgtype, msg) {
    var toastEl = $("#liveToast");
    toastEl.find(".toast-body__text").html(msg);
    if (msgtype === "success") {
      toastEl.find(".toast-body").addClass("text--primary");
      toastEl.find(".toast-body__icon").html(`<i class="bi bi-check-circle-fill"></i>`);
    } else if (msgtype === "error") {
      toastEl.find(".toast-body").addClass("text--error");
      toastEl.find(".toast-body__icon").html(`<i class="bi bi-x-circle-fill"></i>`);
    } else {
      toastEl.find(".toast-body").addClass("text--para");
    }
    toastEl.toast("show");
  }
  // counter homepage
  $(".counter").counterUp({
    delay: 10,
    time: 1000,
  });
});
